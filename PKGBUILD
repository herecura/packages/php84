# Maintainer: Pierre Schmitz <pierre@archlinux.de>

_pkgbase=php
_pkgver=84
pkgbase="${_pkgbase}${_pkgver}"
pkgname=("${pkgbase}"
         "${pkgbase}-dev"
         "${pkgbase}-cgi"
         "${pkgbase}-apache"
         "${pkgbase}-fpm"
         "${pkgbase}-embed"
         "${pkgbase}-phpdbg"
         "${pkgbase}-bcmath"
         "${pkgbase}-bz2"
         "${pkgbase}-calendar"
         "${pkgbase}-dba"
         "${pkgbase}-dblib"
         "${pkgbase}-enchant"
         "${pkgbase}-exif"
         "${pkgbase}-ffi"
         "${pkgbase}-ftp"
         "${pkgbase}-gettext"
         "${pkgbase}-gd"
         "${pkgbase}-gmp"
         "${pkgbase}-iconv"
         "${pkgbase}-intl"
         "${pkgbase}-ldap"
         "${pkgbase}-mysql"
         "${pkgbase}-shmop"
         "${pkgbase}-soap"
         "${pkgbase}-sockets"
         "${pkgbase}-sodium"
         "${pkgbase}-sysvipc"
         "${pkgbase}-odbc"
         "${pkgbase}-pgsql"
         "${pkgbase}-snmp"
         "${pkgbase}-sqlite"
         "${pkgbase}-tidy"
         "${pkgbase}-xsl")
_phppkgver=8.4.5RC1
pkgver=8.4.5rc1
pkgrel=1
arch=('x86_64')
license=('PHP')
url='https://www.php.net/'
makedepends=(
    'phpxx-common-dev' 'apache' 'db' 'gdbm' 'enchant' 'gd'
    'gmp' 'icu' 'libsodium' 'libxslt' 'libzip' 'net-snmp' 'postgresql-libs'
    'sqlite' 'systemd' 'tidy' 'unixodbc' 'curl' 'libtool' 'postfix' 'freetds'
    'pcre2' 'libnsl' 'oniguruma' 'argon2'
)
checkdepends=('procps-ng')
# stable:
#_base_dl_url='https://php.net/distributions/'
# pre release:
_base_dl_url='https://downloads.php.net/~saki/'
#_base_dl_url='https://downloads.php.net/~calvinb/'
source=("${_base_dl_url}${_pkgbase}-${_phppkgver}.tar.xz"{,.asc}
        'apache.patch' 'apache.conf' 'php-fpm.patch' 'php-fpm.tmpfiles' 'php.ini.patch'
    )
sha256sums=('8fd98929129317c5f536f9b7507a3cc29949f4edfc93462989da60e3a3a5c7c1'
            'SKIP'
            '74d33ad3e9fa2f0aaf6ceeddd59ec78d35e66673db74b1053cc3e6105478ba8a'
            '2919aee25e4ba500276535ab1cc0741ccbd39f5d6c20f03dda359d0167f9f106'
            'ff7180de5c613213cc69aa4d774b31e59f9b2379d4e7f93d71150fa9b1879d7b'
            '2f471b3d68864f2eebee5432c83aaa599344b00a13e0322477b892a694b8174b'
            '4392589baef04000c39e947129fd3b371282a146b6f1f4ea045259d93ef72ac6')
validpgpkeys=('0616E93D95AF471243E26761770426E17EBBB3DD'  # Saki Takamachi
              '9D7F99A0CB8F05C8A6958D6256A97AF7600A39A6'  # Calvin Buckley
              'AFD8691FDAEDF03BDF6E460563F15A9B715376CA') # Eric Mann

prepare() {
	cd "${srcdir}/${_pkgbase}-${_phppkgver}"

	patch -p0 -i "${srcdir}/apache.patch"
	patch -p0 -i "${srcdir}/php-fpm.patch"
	patch -p0 -i "${srcdir}/php.ini.patch"
	autoconf

	# Disable failing tests
	rm tests/output/stream_isatty_*.phpt
	rm Zend/tests/arginfo_zpp_mismatch*.phpt
}

build() {
	local _phpconfig="--srcdir=../${_pkgbase}-${_phppkgver} \
		--config-cache \
		--prefix=/usr \
		--sbindir=/usr/bin \
		--sysconfdir=/etc/${pkgbase} \
		--localstatedir=/var \
        --includedir=/usr/include/${pkgbase} \
		--libdir=/usr/lib/${pkgbase} \
		--datarootdir=/usr/share/${pkgbase} \
		--datadir=/usr/share/${pkgbase} \
		--program-suffix=${_pkgver} \
		--with-layout=GNU \
		--with-config-file-path=/etc/${pkgbase} \
		--with-config-file-scan-dir=/etc/${pkgbase}/conf.d \
		--disable-rpath \
		--mandir=/usr/share/man \
        --disable-gcc-global-regs \
		"

	local _phpextensions="\
		--enable-bcmath=shared \
		--enable-calendar=shared \
		--enable-dba=shared \
		--enable-exif=shared \
		--enable-ftp=shared \
		--enable-gd=shared \
		--enable-intl=shared \
		--enable-mbstring \
		--enable-pcntl \
		--enable-shmop=shared \
		--enable-soap=shared \
		--enable-sockets=shared \
		--enable-sysvmsg=shared \
		--enable-sysvsem=shared \
		--enable-sysvshm=shared \
		--with-bz2=shared \
		--with-curl \
		--with-db4=/usr \
		--with-enchant=shared \
		--with-external-gd \
		--with-external-pcre \
		--with-ffi=shared \
		--with-gdbm \
		--with-gettext=shared \
		--with-gmp=shared \
		--with-iconv=shared \
		--with-ldap=shared \
		--with-ldap-sasl \
		--with-mhash \
		--with-mysql-sock=/run/mysqld/mysqld.sock \
		--with-mysqli=shared,mysqlnd \
		--with-openssl \
        --with-openssl-argon2 \
		--with-password-argon2 \
		--with-pdo-dblib=shared,/usr \
		--with-pdo-mysql=shared,mysqlnd \
		--with-pdo-odbc=shared,unixODBC,/usr \
		--with-pdo-pgsql=shared \
		--with-pdo-sqlite=shared \
		--with-pgsql=shared \
		--with-readline \
		--with-snmp=shared \
		--with-sodium=shared \
		--with-sqlite3=shared \
		--with-tidy=shared \
		--with-unixODBC=shared \
		--with-xsl=shared \
		--with-zip \
		--with-zlib \
		"

	EXTENSION_DIR=/usr/lib/${pkgbase}/modules
	export EXTENSION_DIR

	mkdir "${srcdir}/build"
	cd "${srcdir}/build"
	ln -s "../${_pkgbase}-${_phppkgver}/configure"
	./configure ${_phpconfig} \
		--enable-cgi \
		--enable-fpm \
		--with-fpm-systemd \
		--with-fpm-acl \
		--with-fpm-user=http \
		--with-fpm-group=http \
		--enable-embed=shared \
		${_phpextensions}
	make
    mkdir conf-available
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin curl 30 > conf-available/curl.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin 'mail function' 30 > conf-available/mail.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin mbstring 30 > conf-available/mbstring.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin mysqlnd 30 > conf-available/mysqlnd.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin opcache 30 > conf-available/opcache.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin Pdo 30 > conf-available/pdo.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin Phar 30 > conf-available/phar.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        builtin Session 30 > conf-available/session.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        bcmath bcmath > conf-available/bcmath.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        bz2 bz2 > conf-available/bz2.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        calendar calendar > conf-available/calendar.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        dba dba > conf-available/dba.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pdo_dblib pdo_dblib > conf-available/pdo_dblib.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        enchant enchant > conf-available/enchant.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        exif exif > conf-available/exif.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        ffi ffi > conf-available/ffi.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        ftp ftp > conf-available/ftp.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        gd gd > conf-available/gd.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        gettext gettext > conf-available/gettext.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        gmp gmp > conf-available/gmp.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        iconv iconv > conf-available/iconv.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        intl intl > conf-available/intl.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        ldap ldap > conf-available/ldap.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        mysqli MySQLi > conf-available/mysqli.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pdo_mysql Pdo_mysql > conf-available/pdo_mysql.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        odbc ODBC > conf-available/odbc.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pdo_odbc pdo_odbc > conf-available/pdo_odbc.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pgsql PostgreSQL > conf-available/pgsql.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pdo_pgsql pdo_pgsql > conf-available/pdo_pgsql.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        shmop shmop > conf-available/shmop.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        snmp snmp > conf-available/snmp.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        soap soap > conf-available/soap.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sockets sockets > conf-available/sockets.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sodium sodium > conf-available/sodium.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        pdo_sqlite pdo_sqlite > conf-available/pdo_sqlite.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sqlite3 sqlite3 > conf-available/sqlite3.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sysvmsg sysvmsg > conf-available/sysvmsg.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sysvsem sysvsem > conf-available/sysvsem.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        sysvshm sysvshm > conf-available/sysvshm.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        tidy Tidy > conf-available/tidy.ini
    sapi/cli/php \
        /usr/lib/phpxx-common/phpini.php \
        "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" \
        xsl xsl > conf-available/xsl.ini

	# apache
	cp -a "${srcdir}/build" "${srcdir}/build-apache"
	cd "${srcdir}/build-apache"
    make clean
	./configure ${_phpconfig} \
		--with-apxs2 \
		${_phpextensions}
	make

	# phpdbg
	cp -a "${srcdir}/build" "${srcdir}/build-phpdbg"
	cd "${srcdir}/build-phpdbg"
    make clean
	./configure ${_phpconfig} \
		--enable-phpdbg \
		${_phpextensions}
	make
}

check() {
	cd "${srcdir}/build"

	# Check if sendmail was configured correctly (FS#47600)
	sapi/cli/php -n -r 'echo ini_get("sendmail_path");' | grep -q '/usr/bin/sendmail'

	export REPORT_EXIT_STATUS=1
	export NO_INTERACTION=1
	export SKIP_ONLINE_TESTS=1
	export SKIP_SLOW_TESTS=1
	export TEST_PHP_ARGS="-j$(nproc)"
	export TESTS='tests Zend'

	make test
}

package_php84() {
	pkgdesc='A general-purpose scripting language that is especially suited to web development'
	depends=('phpxx-common' 'libxml2' 'curl' 'libzip' 'pcre2' 'argon2' 'oniguruma')
	provides=("${_pkgbase}=${pkgver}")
	backup=("etc/${pkgbase}/php.ini")

	cd "${srcdir}/build"
	make -j1 INSTALL_ROOT="${pkgdir}" install-{modules,cli,programs,pharcmd}
	install -D -m644 "${srcdir}/${_pkgbase}-${_phppkgver}/php.ini-production" "${pkgdir}/etc/${pkgbase}/php.ini"
	install -d -m755 "${pkgdir}/etc/${pkgbase}/conf.d/"
	install -d -m755 "${pkgdir}/etc/${pkgbase}/conf-available/"

    # install builtin modules-available ini
    for inifile in curl.ini mail.ini mbstring.ini mysqlnd.ini opcache.ini pdo.ini phar.ini session.ini; do
        install -D -m644 "${srcdir}/build/conf-available/${inifile}" \
            "${pkgdir}/etc/${pkgbase}/conf-available/${inifile}"
    done

	# remove static modules
	rm -f "${pkgdir}/usr/lib/${pkgbase}/modules/"*.a
	# remove modules provided by sub packages
	rm -f "${pkgdir}/usr/lib/${pkgbase}/modules/"{enchant,gd,intl,mysqli,sodium,odbc,pdo_dblib,pdo_mysql,pdo_odbc,pgsql,pdo_pgsql,pdo_sqlite,shmop,snmp,sysvmsg,sysvsem,sysvshm,sqlite3,tidy,xsl,soap,sockets,bcmath,gmp,bz2,calendar,dba,exif,ffi,ftp,gettext,iconv,ldap}.so
    # remove phpize, this is needed for -dev only
    rm "${pkgdir}/usr/bin/phpize"*
    rm "${pkgdir}/usr/share/man/man1/phpize"*
}

package_php84-dev() {
	pkgdesc='dev files for PHP, needed for extension building'
	depends=("${pkgbase}" 'phpxx-common-dev')

	cd "${srcdir}/build"
	make -j1 INSTALL_ROOT="${pkgdir}" install-{build,headers}

    # install phpize
	install -D -m755 \
        "${srcdir}/build/scripts/phpize" \
        "${pkgdir}/usr/bin/phpize${_pkgver}"
    install -D -m644 \
        "${srcdir}/build/scripts/man1/phpize.1" \
        "${pkgdir}/usr/share/man/man1/phpize${_pkgver}.1"
}

package_php84-cgi() {
	pkgdesc='CGI and FCGI SAPI for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-cgi=${pkgver}")

	cd "${srcdir}/build"
	make -j1 INSTALL_ROOT="${pkgdir}" install-cgi
}

package_php84-apache() {
	pkgdesc='Apache SAPI for PHP'
	depends=("${pkgbase}" 'apache' 'libnsl')
	provides=("${_pkgbase}-apache=${pkgver}")
	backup=("etc/httpd/conf/extra/${pkgbase}_module.conf")

	install -D -m755 ${srcdir}/build-apache/libs/libphp.so ${pkgdir}/usr/lib/httpd/modules/lib${pkgbase}.so
	install -D -m644 ${srcdir}/apache.conf ${pkgdir}/etc/httpd/conf/extra/${pkgbase}_module.conf
}

package_php84-fpm() {
	pkgdesc='FastCGI Process Manager for PHP'
	depends=("${pkgbase}" 'systemd')
	provides=("${_pkgbase}-fpm=${pkgver}")
	backup=("etc/${pkgbase}/php-fpm.conf" "etc/${pkgbase}/php-fpm.d/www.conf")
	options=('!emptydirs')

	cd "${srcdir}/build"
	make -j1 INSTALL_ROOT="${pkgdir}" install-fpm
	install -D -m644 sapi/fpm/php-fpm.service "${pkgdir}/usr/lib/systemd/system/${pkgbase}-fpm.service"
	install -D -m644 "${srcdir}/php-fpm.tmpfiles" "${pkgdir}/usr/lib/tmpfiles.d/${pkgbase}-fpm.conf"
}

package_php84-embed() {
	pkgdesc='Embedded PHP SAPI library'
	depends=("${pkgbase}" 'systemd-libs' 'libnsl' 'libxcrypt')
	provides=("${_pkgbase}-embed=${pkgver}")
	options=('!emptydirs')

	cd "${srcdir}/build"
	make -j1 INSTALL_ROOT="${pkgdir}" PHP_SAPI=embed install-sapi
}

package_php84-phpdbg() {
	pkgdesc='Interactive PHP debugger'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-phpdbg=${pkgver}")
	options=('!emptydirs')

	cd "${srcdir}/build-phpdbg"
	make -j1 INSTALL_ROOT="${pkgdir}" install-phpdbg
}

package_php84-bcmath() {
	pkgdesc='BCMath Arbitrary Precision Mathematics module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-bcmath=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/bcmath.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/bcmath.so"
    install -D -m644 "${srcdir}/build/conf-available/bcmath.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/bcmath.ini"
}

package_php84-bz2() {
	pkgdesc='Bzip2 module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-bz2=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/bz2.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/bz2.so"
    install -D -m644 "${srcdir}/build/conf-available/bz2.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/bz2.ini"
}

package_php84-calendar() {
	pkgdesc='Calendar module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-calendar=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/calendar.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/calendar.so"
    install -D -m644 "${srcdir}/build/conf-available/calendar.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/calendar.ini"
}

package_php84-dba() {
	pkgdesc='Database (dbm-style) Abstraction Layer module for PHP'
	depends=("${pkgbase}" 'db' 'gdbm')
	provides=("${_pkgbase}-dba=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/dba.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/dba.so"
    install -D -m644 "${srcdir}/build/conf-available/dba.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/dba.ini"
}

package_php84-dblib() {
	pkgdesc='dblib module for PHP'
	depends=("${pkgbase}" 'freetds')
	provides=("${_pkgbase}-dblib=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/pdo_dblib.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pdo_dblib.so"
    install -D -m644 "${srcdir}/build/conf-available/pdo_dblib.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/pdo_dblib.ini"
}

package_php84-enchant() {
	pkgdesc='enchant module for PHP'
	depends=("${pkgbase}" 'enchant')
	provides=("${_pkgbase}-enchant=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/enchant.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/enchant.so"
    install -D -m644 "${srcdir}/build/conf-available/enchant.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/enchant.ini"
}

package_php84-exif() {
	pkgdesc='Exchangeable image information module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-exif=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/exif.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/exif.so"
    install -D -m644 "${srcdir}/build/conf-available/exif.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/exif.ini"
}

package_php84-ffi() {
	pkgdesc='Foreign Function Interface module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-ffi=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/ffi.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/ffi.so"
    install -D -m644 "${srcdir}/build/conf-available/ffi.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/ffi.ini"
}

package_php84-ftp() {
	pkgdesc='FTP module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-ftp=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/ftp.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/ftp.so"
    install -D -m644 "${srcdir}/build/conf-available/ftp.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/ftp.ini"
}

package_php84-gettext() {
	pkgdesc='Gettext module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-gettext=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/gettext.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/gettext.so"
    install -D -m644 "${srcdir}/build/conf-available/gettext.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/gettext.ini"
}

package_php84-gd() {
	pkgdesc='gd module for PHP'
	depends=("${pkgbase}" 'gd')
	provides=("${_pkgbase}-gd=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/gd.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/gd.so"
    install -D -m644 "${srcdir}/build/conf-available/gd.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/gd.ini"
}

package_php84-gmp() {
	pkgdesc='GNU Multiple Precision module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-gmp=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/gmp.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/gmp.so"
    install -D -m644 "${srcdir}/build/conf-available/gmp.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/gmp.ini"
}

package_php84-iconv() {
	pkgdesc='iconv module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-iconv=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/iconv.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/iconv.so"
    install -D -m644 "${srcdir}/build/conf-available/iconv.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/iconv.ini"
}

package_php84-intl() {
	pkgdesc='intl module for PHP'
	depends=("${pkgbase}" 'icu')
	provides=("${_pkgbase}-intl=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/intl.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/intl.so"
    install -D -m644 "${srcdir}/build/conf-available/intl.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/intl.ini"
}

package_php84-ldap() {
	pkgdesc='LDAP module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-ldap=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/ldap.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/ldap.so"
    install -D -m644 "${srcdir}/build/conf-available/ldap.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/ldap.ini"
}

package_php84-mysql() {
	pkgdesc='MySQL modules for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-mysql=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/mysqli.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/mysqli.so"
	install -D -m755 "${srcdir}/build/modules/pdo_mysql.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pdo_mysql.so"
    install -D -m644 "${srcdir}/build/conf-available/mysqli.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/mysqli.ini"
    install -D -m644 "${srcdir}/build/conf-available/pdo_mysql.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/pdo_mysql.ini"
}

package_php84-odbc() {
	pkgdesc='ODBC modules for PHP'
	depends=("${pkgbase}" 'unixodbc')
	provides=("${_pkgbase}-odbc=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/odbc.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/odbc.so"
	install -D -m755 "${srcdir}/build/modules/pdo_odbc.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pdo_odbc.so"
    install -D -m644 "${srcdir}/build/conf-available/odbc.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/odbc.ini"
}

package_php84-pgsql() {
	pkgdesc='PostgreSQL modules for PHP'
	depends=("${pkgbase}" 'postgresql-libs')
	provides=("${_pkgbase}-pgsql=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/pgsql.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pgsql.so"
	install -D -m755 "${srcdir}/build/modules/pdo_pgsql.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pdo_pgsql.so"
    install -D -m644 "${srcdir}/build/conf-available/pgsql.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/pgsql.ini"
    install -D -m644 "${srcdir}/build/conf-available/pdo_pgsql.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/pdo_pgsql.ini"
}

package_php84-shmop() {
	pkgdesc='Shared Memory module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-shmop=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/shmop.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/shmop.so"
    install -D -m644 "${srcdir}/build/conf-available/shmop.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/shmop.ini"
}

package_php84-snmp() {
	pkgdesc='snmp module for PHP'
	depends=("${pkgbase}" 'net-snmp')
	provides=("${_pkgbase}-snmp=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/snmp.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/snmp.so"
    install -D -m644 "${srcdir}/build/conf-available/snmp.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/snmp.ini"
}

package_php84-soap() {
	pkgdesc='Soap module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-soap=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/soap.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/soap.so"
    install -D -m644 "${srcdir}/build/conf-available/soap.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/soap.ini"
}

package_php84-sockets() {
	pkgdesc='sockets module for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-sockets=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/sockets.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sockets.so"
    install -D -m644 "${srcdir}/build/conf-available/sockets.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sockets.ini"
}

package_php84-sodium() {
	pkgdesc='sodium module for PHP'
	depends=("${pkgbase}" 'libsodium')
	provides=("${_pkgbase}-sodium=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/sodium.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sodium.so"
    install -D -m644 "${srcdir}/build/conf-available/sodium.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sodium.ini"
}

package_php84-sqlite() {
	pkgdesc='sqlite module for PHP'
	depends=("${pkgbase}" 'sqlite')
	provides=("${_pkgbase}-sqlite=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/sqlite3.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sqlite3.so"
	install -D -m755 "${srcdir}/build/modules/pdo_sqlite.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/pdo_sqlite.so"
    install -D -m644 "${srcdir}/build/conf-available/sqlite3.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sqlite3.ini"
    install -D -m644 "${srcdir}/build/conf-available/pdo_sqlite.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/pdo_sqlite.ini"
}

package_php84-sysvipc() {
	pkgdesc='Sys V IPC modules for PHP'
	depends=("${pkgbase}")
	provides=("${_pkgbase}-sysvipc=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/sysvmsg.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sysvmsg.so"
	install -D -m755 "${srcdir}/build/modules/sysvsem.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sysvsem.so"
	install -D -m755 "${srcdir}/build/modules/sysvshm.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/sysvshm.so"
    install -D -m644 "${srcdir}/build/conf-available/sysvmsg.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sysvmsg.ini"
    install -D -m644 "${srcdir}/build/conf-available/sysvsem.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sysvsem.ini"
    install -D -m644 "${srcdir}/build/conf-available/sysvshm.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/sysvshm.ini"
}

package_php84-tidy() {
	pkgdesc='tidy module for PHP'
	depends=("${pkgbase}" 'tidy')
	provides=("${_pkgbase}-tidy=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/tidy.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/tidy.so"
    install -D -m644 "${srcdir}/build/conf-available/tidy.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/tidy.ini"
}

package_php84-xsl() {
	pkgdesc='xsl module for PHP'
	depends=("${pkgbase}" 'libxslt')
	provides=("${_pkgbase}-xsl=${pkgver}")

	install -D -m755 "${srcdir}/build/modules/xsl.so" \
        "${pkgdir}/usr/lib/${pkgbase}/modules/xsl.so"
    install -D -m644 "${srcdir}/build/conf-available/xsl.ini" \
        "${pkgdir}/etc/${pkgbase}/conf-available/xsl.ini"
}
